angular.module('starter.controllers', [])

.controller('actionCtrl', function($scope,$rootScope, $ionicActionSheet, $ionicPopup, $timeout, $state,$firebaseArray,CurrentDate,$firebaseObject,$firebaseAuth,$ionicLoading) {

  
  var societyRef="societies/"+$rootScope.userData.societyName;
  var announcementsRef=firebase.database().ref().child(societyRef).child("announcements").orderByChild("timestamp");
  var complaintsRef=firebase.database().ref().child(societyRef).child("complaints").orderByChild("timestamp");
  $scope.announcements=$firebaseArray(announcementsRef);
  $scope.complaints=$firebaseArray(complaintsRef);

  
  $scope.showActionsheet = function() {
    
    $ionicActionSheet.show({
      titleText: 'Select an option',
      buttons: [
        { text: 'Join as an Administrator' },
      ],
      destructiveText: 'Logout',
      cancelText: 'Cancel',
      cancel: function() {
        console.log('CANCELLED');
      },
      buttonClicked: function(index) {
        console.log('BUTTON CLICKED', index);
        if(index == 0) {
          $state.go('admin');
        }
        else {
          return true; //logout
        }
        return true;
      },
      destructiveButtonClicked: function() {
        var auth=$firebaseAuth().$signOut();
        showProgressIcon("Logging out.....");
        setTimeout(function(){
          hideProgressIcon();
          $state.go('login'); }
          ,3000);
        
        return true;
      }
    });
  };

  $scope.addAnnouncement = function() {
   
    var myPopup = $ionicPopup.show({
    title: 'Post an Announcement',
    template: '<input type="text" placeholder="Type an Announcement Subject" ng-model="data.annSubject"> <br> <textarea placeholder="Type your Announcement here" rows="8" cols="10" ng-model="data.annContent">',
    scope: $scope,
    buttons: [
      { text: 'Cancel' },
      {
        text: '<b>Post</b>',
        type: 'button-positive',
        onTap: function(e) {
          var annSubject=this.scope.data.annSubject;
          var annContent=this.scope.data.annContent;
          var userName=$rootScope.userData.firstname + ' ' + $rootScope.userData.lastname;
          var upvotelist=[];
          var annObject={"subject" : annSubject,
          "content" : annContent,
          "userName" : userName,
          "date" : CurrentDate,
          "timestamp" : -Math.floor(Date.now()/1000),
          "upvotes" : 0,
           "uid":$rootScope.userData.uid
          }
          $scope.announcements.$add(annObject).then(function(announcementsRef){
            var annId=announcementsRef.key;
            var annObj=$scope.announcements.$getRecord(annId);
            annObj.annId=annId;
            $scope.announcements.$save(annObj);
          });
      
          // if (!$scope.data.wifi) {
          //   e.preventDefault();
          // } else {
          //   return $scope.data.subject;
          // }
        }
      }
    ]
  });

  myPopup.then(function(res) {
    console.log('Tapped!', res);
  });


  }

  $scope.addHelpDesk = function() {
    var myPopup = $ionicPopup.show({
    title: 'Post a Complaint',
    template: '<input type="text" placeholder="Type a Complaint Subject" ng-model="data.compSubject"> <br> <textarea placeholder="Type your Complaint here" rows="8" cols="10" ng-model="data.compContent">',
    scope: $scope,
    buttons: [
      { text: 'Cancel' },
      {
        text: '<b>Post</b>',
        type: 'button-positive',
        onTap: function(e) {
          var compSubject=this.scope.data.compSubject;
          var compContent=this.scope.data.compContent;
          var userName=$rootScope.userData.firstname + ' ' + $rootScope.userData.lastname;
          var upvotelist=[];
          var compObj={
            "subject" : compSubject,
            "content" : compContent,
            "userName" : userName,
            "uid" : $rootScope.userData.uid,
            "date" : CurrentDate,
            "timestamp" : -Math.floor(Date.now()/1000),
            "upvotes" : 0,
            "status" : "",
            "adminComment" : "",
            "uid":$rootScope.userData.uid
          }
          $scope.complaints.$add(compObj).then(function(complaintsRef){
            var compId= complaintsRef.key;
            var compObject = $scope.complaints.$getRecord(compId);
            compObject.compId= compId;
            $scope.complaints.$save(compObject);

          });


        }
      }
    ]
  });

  myPopup.then(function(res) {
    console.log('Tapped!', res);
  });

 
  }

  var showProgressIcon=function(message){
    $ionicLoading.show({
      template: message
    }).then(function(){
       console.log("The loading indicator is now displayed");
    });
  }


  var hideProgressIcon=function(){
    $ionicLoading.hide().then(function(){
       console.log("The loading indicator is now hidden");
    });
  }


})

.controller('annCtrl', function($scope,$rootScope, $ionicPopup, $ionicActionSheet, $timeout,$stateParams,$firebaseArray,$state,$ionicPopup) {


    var societyRef="societies/"+$rootScope.userData.societyName;
    var announcementsRef=firebase.database().ref().child(societyRef).child("announcements").orderByChild("timestamp");
     $scope.announcements=$firebaseArray(announcementsRef);

    $scope.annSubject=$stateParams.annSubject;
    $scope.annContent=$stateParams.annContent;
    $scope.annId=$stateParams.annId;
    $scope.uid=$stateParams.uid;

     $scope.isOwner=false;
    if($rootScope.userData.isAdmin || $stateParams.uid==$rootScope.userData.uid){
      $scope.isOwner=true;
    }
  
    


    function checkUid(upvoteList){
      if(upvoteList.indexOf($rootScope.userData.uid))
        return true;
      else
        return false;

    }

    $scope.like = function() {
    var annObj=$scope.announcements.$getRecord($scope.annId);
    var upvotelist=annObj.upvotelist;
    var isPresent=false;
    if(upvotelist==undefined){

      upvotelist=[];
      upvotelist.push($rootScope.userData.uid);
      annObj.upvotelist=upvotelist;
      annObj.upvotes=annObj.upvotes+1;
      $scope.announcements.$save(annObj);
      showErrorPopUp("Upvote added");
    }else{
      if(checkUid(upvotelist)){

        upvotelist.push($rootScope.userData.uid);
        annObj.upvotelist=upvotelist;
        annObj.upvotes=annObj.upvotes+1;
        $scope.announcements.$save(annObj);
        showErrorPopUp("Upvote added");
      }else{
        index=upvotelist.indexOf($rootScope.userData.uid);
        upvotelist.splice(index,1);
         annObj.upvotelist=upvotelist;
         annObj.upvotes=annObj.upvotes-1;
          $scope.announcements.$save(annObj);
        showErrorPopUp("Upvote Removed");
      }
      
    }
  }

  $scope.showActionsheet = function() {
    
    $ionicActionSheet.show({
      titleText: 'Select an option',
      buttons: [
        { text: 'Edit' },
        { text: 'Delete' }
      ],
      cancelText: 'Cancel',
      cancel: function() {
        console.log('CANCELLED');
      },
      buttonClicked: function(index) {
        console.log('BUTTON CLICKED', index);
        if(index == 0) {

          var myPopup = $ionicPopup.show({
          title: 'Edit an Announcement',
          template: '<input type="text" placeholder="Edit your Announcement Subject" ng-model="data.editSubject"> <br> <textarea placeholder="Edit your Announcement here" rows="8" cols="10" ng-model="data.editContent">',
          scope: $scope,
          buttons: [
            { text: 'Cancel' },
            {
              text: '<b>Post</b>',
              type: 'button-positive',
              onTap: function(e) {
                 var annObj=$scope.announcements.$getRecord($scope.annId);
                annObj.subject=this.scope.data.editSubject;
                annObj.content=this.scope.data.editContent;
                $scope.announcements.$save(annObj);
                $state.go('home');
              }
            }
          ]
        });
        myPopup.then(function(res) {
          console.log('Tapped!', res);
        });

       
      }

    else {
      var confirmPopup = $ionicPopup.confirm({
          title: 'Delete',
          template: 'Are you sure you want to delete this announcement?'
        });
      confirmPopup.then(function(res) {
       if(res) {
               $scope.announcements.$remove($scope.announcements.$getRecord($scope.annId)).then(function(ref){
                
               },function(error){
                console.log(error);
               });


           $state.go('home');

         
       } else {
         console.log('You are not sure to delete');
       }
      });
    }

    return true;
      },
    });
  };

  var showErrorPopUp=function(title,message){
    var alertPopup = $ionicPopup.alert({
      title: title,
      template: message,
   });
   alertPopup.then(function(res) {
      console.log('Thanks');
   });
  }

})


.controller('helpdeskCtrl', function($scope, $ionicPopup, $ionicActionSheet, $timeout,$stateParams,$firebaseArray,$rootScope,$state,$firebaseObject,$ionicPopup) {


     var societyRef="societies/"+$rootScope.userData.societyName;
    $scope.compSubject=$stateParams.compSubject;
    $scope.compContent=$stateParams.compContent;
    $scope.compId=$stateParams.compId;
    $scope.compStatus=$stateParams.compStatus;
    var commentObj=$firebaseObject(firebase.database().ref().child(societyRef).child("complaints").child($scope.compId).child("adminComment"));
    commentObj.$bindTo($scope,"adminComment").then(function(){
      console.log($scope.adminComment);
    });

    var statusObj=$firebaseObject(firebase.database().ref().child(societyRef).child("complaints").child($scope.compId).child("status"));
    statusObj.$bindTo($scope,"status").then(function(){
      console.log($scope.status);
    });

    var complaintsRef=firebase.database().ref().child(societyRef).child("complaints").orderByChild("timestamp");
    $scope.complaints=$firebaseArray(complaintsRef);



    $scope.isOwner=false;
    if($rootScope.userData.isAdmin || $stateParams.uid==$rootScope.userData.uid){
      $scope.isOwner=true;
    }



    function checkUid(upvoteList){
      if(upvoteList.indexOf($rootScope.userData.uid))
        return true;
      else
        return false;

    }

    $scope.like = function() {
     var compObj=$scope.complaints.$getRecord($scope.compId);
    var upvotelist=compObj.upvotelist;
    var isPresent=false;

    if(upvotelist==undefined){
      upvotelist=[];
      upvotelist.push($rootScope.userData.uid);
      compObj.upvotelist=upvotelist;
      compObj.upvotes=compObj.upvotes+1;
      $scope.complaints.$save(compObj);
      showErrorPopUp("Upvoted added");
    }else{
      if(checkUid(upvotelist)){
        upvotelist.push($rootScope.userData.uid);
        compObj.upvotelist=upvotelist;
        compObj.upvotes=compObj.upvotes+1;
        $scope.complaints.$save(compObj);
        showErrorPopUp("Upvoted added");
      }else{
        index=upvotelist.indexOf($rootScope.userData.uid);
        upvotelist.splice(index,1);
         compObj.upvotelist=upvotelist;
         compObj.upvotes=compObj.upvotes-1;
          $scope.complaints.$save(compObj);
        showErrorPopUp("Upvoted removed");
      }
    }
  }

  $scope.comment = function() {
    var myPopup = $ionicPopup.show({
      templateUrl: 'selectOption.html',
      title: 'Change Status',
      scope: $scope,
      buttons: [{
        text: 'Yes',
        type: 'button-positive',
        onTap: function (e) {
          var compObj=$scope.complaints.$getRecord($scope.compId);
          compObj.status=this.scope.choice.option;
          $scope.complaints.$save(compObj);

        }
      }, {
        text: 'No',
        type: 'button-default',
        onTap: function (e) {
          return true;
        }
      }]
    });
  }

  $scope.changeStatus = function() {
    var myPopup = $ionicPopup.show({
    title: 'Post a Comment',
    template: '<textarea placeholder="Type your comment here" rows="8" cols="10" ng-model="data.adminComment"> ',
    scope: $scope,
    buttons: [
      { text: 'Cancel' },
      {
        text: '<b>Post</b>',
        type: 'button-positive',
        onTap: function(e) {
            var compObj=$scope.complaints.$getRecord($scope.compId);
            compObj.adminComment=this.scope.data.adminComment;
            $scope.complaints.$save(compObj);
            

        }
      }
    ]
  });

  myPopup.then(function(res) {
    console.log('Tapped!', res);
  });

 
  }

 





  $scope.showActionsheet = function() {
    
    $ionicActionSheet.show({
      titleText: 'Select an option',
      buttons: [
        { text: 'Edit' },
        { text: 'Delete' }
      ],
      cancelText: 'Cancel',
      cancel: function() {
        console.log('CANCELLED');
      },
      buttonClicked: function(index) {
        console.log('BUTTON CLICKED', index);
        if(index == 0) {
          var myPopup = $ionicPopup.show({
          title: 'Edit a Complaint',
          template: '<input type="text" placeholder="Type an Complaint Subject" ng-model="data.editSub"> <br> <textarea placeholder="Type your Complaint here" rows="8" cols="10" ng-model="data.editCont">',
          scope: $scope,
          buttons: [
            { text: 'Cancel' },
            {
              text: '<b>Post</b>',
              type: 'button-positive',
              onTap: function(e) {
                var compObj=$scope.complaints.$getRecord($scope.compId);
                compObj.subject=this.scope.data.editSub;
                compObj.content=this.scope.data.editCont;
                $scope.complaints.$save(compObj);
              
                $state.go('home');
              }
            }
          ]
        });

        myPopup.then(function(res) {
          console.log('Tapped!', res);
        });

        
      }

    else {
      var confirmPopup = $ionicPopup.confirm({
          title: 'Delete',
          template: 'Are you sure you want to delete this announcement?'
        });
      confirmPopup.then(function(res) {
       if(res) {
         $scope.complaints.$remove($scope.complaints.$getRecord($scope.compId)).then(function(ref){
               },function(error){
                console.log(error);
               });
       } else {
         console.log('You are not sure complaint');
       }
      });

       $state.go('home');
    }

    return true;

      },
    });
  };

  var showErrorPopUp=function(title,message){
    var alertPopup = $ionicPopup.alert({
      title: title,
      template: message,
   });
   alertPopup.then(function(res) {
      console.log('Thanks');
   });
  }


})

.controller('signupCtrl',function($scope,$firebaseAuth,$firebaseObject,$ionicLoading,$state,$ionicPopup,$firebaseArray){
  var auth=$firebaseAuth();

  $scope.societyNames=[];
  var socRef=firebase.database().ref().child("societies");
  var socs=$firebaseArray(socRef);
  socs.$loaded().then(function(data){
    var key=data.forEach(function(child){
        $scope.societyNames.push(child.$id);
    });
  })

  $scope.signup=function(email,password,firstname,lastname,societyName,flatNumber,confirmPassword){
    
      showProgressIcon("Creating account..")
     verifyAndCreateAccount(email,password,firstname,lastname,societyName,flatNumber,confirmPassword);
  
  };


  var verify=function(email,password,firstname,lastname,societyName,flatNumber,confirmPassword){
      if(email==undefined){
          return("Invalid email address");
      }
      else if(email==undefined || password==undefined  || firstname==undefined  || lastname==undefined  || societyName==undefined
        ||flatNumber==undefined  ||   confirmPassword==undefined){
        return("Please fill all fields");
      }else if(password.length<6){
        return ("Password must have more than 6 characters");
      }else if(password!=confirmPassword){
        return("Password and confirm password do not match");
      }else{
        return(null);
      }
  }

  var verifyAndCreateAccount=function(email,password,firstname,lastname,societyName,flatNumber,confirmPassword){
     var verifyMessage=verify(email,password,firstname,lastname,societyName,flatNumber,confirmPassword);
      if(verifyMessage==null){
     
     auth.$createUserWithEmailAndPassword(email,password)
      .then(function(firebaseUser){
        var ref=firebase.database().ref().child("users").child(firebaseUser.uid);
          var obj=$firebaseObject(ref);
          obj.email=email;
          obj.password=password;
          obj.firstname=firstname;
          obj.lastname=lastname;
          obj.flatNumber=flatNumber;
          obj.uid=firebaseUser.uid;
          obj.societyName=societyName;
          obj.isAdmin=false;
          obj.$save();
          hideProgressIcon();
          $state.go('login');     
      }).catch(function(error){
        $scope.error=error;
        console.log(error);
        hideProgressIcon();
        showErrorPopUp(error.message)
      });
   } else{
    hideProgressIcon();
    showErrorPopUp(verifyMessage);
   }
  }
  
  

  var showProgressIcon=function(message){
    $ionicLoading.show({
      template: message
    }).then(function(){
       console.log("The loading indicator is now displayed");
    });
  }


  var hideProgressIcon=function(){
    $ionicLoading.hide().then(function(){
       console.log("The loading indicator is now hidden");
    });
  }

   var showErrorPopUp=function(title,message){
    var alertPopup = $ionicPopup.alert({
      title: title,
      template: message,
   });

   alertPopup.then(function(res) {
      console.log('Thanks');
   });
  }



 
})

.controller('loginCtrl',function($scope,$rootScope,$firebaseAuth,$state,$firebaseObject,$ionicLoading,$ionicPopup){
  var auth=$firebaseAuth();

  auth.$onAuthStateChanged(function(firebaseUser){
    if(firebaseUser){
      showProgressIcon('Signing in..')
       var  userObj=$firebaseObject(firebase.database().ref().child("users").child(firebaseUser.uid));
          userObj.$loaded(function(data){
            $rootScope.userData=data;
           hideProgressIcon();
             $state.go('home');
          });
          console.log("User signed in with uid: " + firebaseUser.uid);
    }else{
      console.log('not logged in');
    }
  });



    
  $scope.signin=function(){
     
      auth.$signInWithEmailAndPassword(this.email, this.password)
        .then(function(firebaseUser) {
          showProgressIcon("Authenticating...");
          var  userObj=$firebaseObject(firebase.database().ref().child("users").child(firebaseUser.uid));
          userObj.$loaded(function(data){
            $rootScope.userData=data;
           hideProgressIcon();
             $state.go('home');
          });
          console.log("User signed in with uid: " + firebaseUser.uid);
         
        }).catch(function(error) {
           $scope.error = error;
           var errorMessage="Invalid email or password"
           showErrorPopUp("Authentication Error",errorMessage);
          
        });


         

  };

  var showProgressIcon=function(message){
    $ionicLoading.show({
      template: message
    }).then(function(){
       console.log("The loading indicator is now displayed");
    });
  }

  var hideProgressIcon=function(){
    $ionicLoading.hide().then(function(){
       console.log("The loading indicator is now hidden");
    });
  }

  var showErrorPopUp=function(title,message){
    var alertPopup = $ionicPopup.alert({
      title: title,
      template: message,
   });

   alertPopup.then(function(res) {
      console.log('Thanks');
   });
  }


})

.controller('adminCtrl',function($scope,$firebaseObject,$firebaseAuth,$rootScope,$state,$ionicLoading,$ionicPopup){
  $scope.verify=function(socName,socPassword){
   
    if($rootScope.userData.isAdmin){
      showErrorPopUp("You are already an admin");
      $state.go('home');
      return;
    }
    var pwdRef=firebase.database().ref().child("societies").child(socName).child("adminPassword");
    var pwdObj=$firebaseObject(pwdRef);
    pwdObj.$loaded(function(data){
      if(socPassword==data.$value && $rootScope.userData.isAdmin==false){
        var userObjRef=firebase.database().ref().child("users").child($rootScope.userData.uid);
        userObjRef.update({'isAdmin': true});
        updateAdminList();
        hideProgressIcon();
        showErrorPopUp("You have been verified successfully");
        $state.go('home');
      }else{
        console.log('failed to authenticate');
        hideProgressIcon();
        showErrorPopUp("Failed to authenticate.Please check your society name and password");
      }
    })


   var updateAdminList=function(){
    showProgressIcon("Authenticating...")
      var societyRef=firebase.database().ref().child("societies").child($rootScope.userData.societyName);
    $scope.society=$firebaseObject(societyRef);
    $scope.society.$loaded(function(data){
      if(data.adminList==null){
        var adminList=[];
        adminList.push($rootScope.userData.uid);
        societyRef.update({'adminList':adminList});
        console.log('Admin list inserted');
      }else{
        var adminList=data.adminList;
        adminList.push($rootScope.userData.uid);
        societyRef.update({'adminList':adminList});
        console.log('Admin List updated');
       
      }
      })
   }

  } 


   var showProgressIcon=function(message){
    $ionicLoading.show({
      template: message
    }).then(function(){
       console.log("The loading indicator is now displayed");
    });
  }

   var hideProgressIcon=function(){
    $ionicLoading.hide().then(function(){
       console.log("The loading indicator is now hidden");
    });
  }

  var showErrorPopUp=function(title,message){
    var alertPopup = $ionicPopup.alert({
      title: title,
      template: message,
   });
   alertPopup.then(function(res) {
      console.log('Thanks');
   });
  }
})